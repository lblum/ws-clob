/**
 * 
 */
package ar.com.menhir.db;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;

public class WSProperties {

	private static Properties mProps = null;

	private static void loadProperties() throws Exception {
		mProps = new Properties();
		String strSQL = "select * from web_ws_param";
		Statement s = DBConn.openConn().createStatement();
		ResultSet rs = s.executeQuery(strSQL);
		String strKey, strVal;
		while (rs.next()) {
			strKey = rs.getString("V_param_Name");
			strVal = rs.getString("V_param_Val");
			mProps.put(strKey, strVal);
		}
		rs.close();
		s.close();
	
	}

	public static String getPropertyVal(String key) {
		return getPropertyVal(key, "");
	}

	public static String getPropertyVal(String key, String defaultVal) {
		String retVal = null;
		try {
			if (mProps == null)
				loadProperties();

			retVal = mProps.getProperty(key);
		} catch (Exception e) {
			retVal = null;
		}
		retVal = retVal == null ? defaultVal : retVal;
		return retVal; 
	}

	public static String getDBURL() throws Exception {
		return System.getProperty("dbURL", null);
	}

	public static String getDBDriver() throws Exception {
		return System.getProperty("dbDriver", "oracle.jdbc.driver.OracleDriver");		                                       
	}

	public static String getDBUser() throws Exception {
		return System.getProperty("dbUser", null);
	}

	public static String getDBPass() throws Exception {
		return System.getProperty("dbPass", null);
	}

	public static String getCertFile() {
		return getPropertyVal("cert.fileName");
	}

	public static String getCertPass() {
		return getPropertyVal("cert.filePass");
	}

	public static String getTCPDebug() {
		return getPropertyVal("javax.net.debug", "all");
	}

	public static Level getLogLevel() {
		String strLevel = getPropertyVal("logLevel", "SEVERE");
		
		Level retVal;
		
		try {
			retVal = Level.parse(strLevel);
		} catch(Exception e){
			retVal = Level.SEVERE;
		}
		
		return retVal;
	}
	
	public static String getWSAAEndpoint() {
		// TODO Auto-generated method stub
		return getWSEndpoint("wsaa");
	}

	public static String getWSEndpoint(String service) {
		return getPropertyVal(service + ".endpoint");
	}

	public static long getCuit() {
		return Long.parseLong(getPropertyVal("cuit"));
	}
	
	public static String getTokenTag(String service) {
		return getPropertyVal(service + ".tokenTag","Token");
	}
	
	public static String getAuthTag(String service) {
		return getPropertyVal(service + ".authTag","Auth");
	}
	public static String getSignTag(String service) {
		return getPropertyVal(service + ".signTag","Sign");
	}
	
	public static String getCuitTag(String service) {
		return getPropertyVal(service + ".cuitTag","Cuit");
	}
}
