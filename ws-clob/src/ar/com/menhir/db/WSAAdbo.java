/**
 * 
 */
package ar.com.menhir.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

import ar.com.menhir.WSAA.Login;

/**
 * @author lblum
 *
 */
public class WSAAdbo extends Login {
	final static Logger logger = Logger.getLogger(WSAAdbo.class);
	

	/**
	 * @param certFile
	 * @param passwd
	 * @throws Exception
	 */
	public WSAAdbo(String service) throws Exception {
		super(WSProperties.getCertFile(), WSProperties.getCertPass());
		setEndPoint(WSProperties.getWSAAEndpoint());
		setService(service);
		setCUIT(WSProperties.getCuit());
	}

	private void loadFromDB() throws Exception {

		String selSQL = "select *\n" + "  from web_ws_wsaa\n" + " where upper(v_servicio) = '" + getService().toUpperCase()
				+ "' \n" + "   and fec_expira>sysdate";
		logger.debug(selSQL);
		Statement s = DBConn.openConn().createStatement();
		ResultSet rs = s.executeQuery(selSQL);
		if (rs.next()) {
			// Habia token

			this.mSign = rs.getString("v_sign");
			this.mToken = rs.getString("v_token");
			this.mExpDate = rs.getTimestamp("fec_expira");

		}
		rs.close();
		s.close();
	}

	private void saveToDB() throws Exception {
		String SQL = "delete from web_ws_wsaa where upper(v_servicio) = ?";
		PreparedStatement ps = DBConn.openConn().prepareStatement(SQL);
		ps.setString(1, getService().toUpperCase());
		ps.execute();
		ps.close();

		SQL = "insert into web_ws_wsaa (v_servicio,v_token,v_sign,fec_expira) values (?,?,?,?)";
		logger.debug(SQL);
		ps = DBConn.openConn().prepareStatement(SQL);
		ps.setString(1, this.getService());
		ps.setString(2, this.mToken);
		ps.setString(3, mSign);
		ps.setTimestamp(4, new java.sql.Timestamp(this.mExpDate.getTime()));
		ps.execute();
		ps.close();
	}

	protected void doLogin() throws Exception {
		// Primero lo busco en la base de datos
		loadFromDB();
		if (isExpired()) {
			logger.debug("Token expirado o inexistente");
			super.doLogin();
			saveToDB();
		}
	}
	
	public String getAuth() throws Exception {
		doLogin();
		System.out.println(this.getService());
		
		String tokenTag = WSProperties.getTokenTag(this.getService());
		String signTag = WSProperties.getSignTag(this.getService());
		String cuitTag = WSProperties.getCuitTag(this.getService());
		String authTag = WSProperties.getAuthTag(this.getService());
		
		String strOut = "<" +  tokenTag + ">" + mToken + "</" + tokenTag+ ">";
		strOut += "<" + signTag + ">" + mSign + "</" + signTag + ">";
		strOut += "<" + cuitTag + ">" + WSProperties.getCuit() + "</" + cuitTag + ">";
		return "<" + authTag + ">" +strOut + "</" + authTag + ">";
	}
	
}
