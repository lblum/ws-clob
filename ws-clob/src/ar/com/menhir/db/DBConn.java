/**
 * 
 */
package ar.com.menhir.db;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;

/**
 * @author lblum
 *
 */
public class DBConn {
	static Connection mConn = null;

	final static Logger logger = Logger.getLogger(DBConn.class);

	public static Connection openConn() throws Exception {
		if (mConn == null) {
			
			logger.info("Conectando a la base de datos");
			String dbDriver =WSProperties.getDBDriver(); 
			String dbURL = WSProperties.getDBURL();
			Class.forName(dbDriver);
			
			
			mConn = DriverManager.getConnection(dbURL, WSProperties.getDBUser(),
					WSProperties.getDBPass());
			mConn.setAutoCommit(true);
			logger.info("Conecci�n a la base de datos OK");
		}
		return mConn;
	}

	public static void closeConn() throws Exception {
		if (mConn != null) {
			mConn.close();
			mConn = null;
		}
	}

	// El registro corriente
	private static Object currRec = null;

	// La PK corriente del logger
	private static long currSeq = -1;

	public static long getCurrSeq() {
		return currSeq;
	}

	public static void setCurrRec(Object currRec) {
		DBConn.currRec = currRec;
	}

}
