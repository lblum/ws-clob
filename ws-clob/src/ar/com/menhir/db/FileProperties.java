package ar.com.menhir.db;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

public class FileProperties {
	static FileInputStream findFile (String fileName )throws Exception {
		File iniFile=new File(fileName);
		return new FileInputStream(iniFile);
		
	}
	
	public static void loadPropFile(String fileName) throws Exception {
		Properties mProps=new Properties();
		FileInputStream iniFile = new FileInputStream(fileName);
		try {
			mProps.load(iniFile);
			iniFile.close();
		    @SuppressWarnings("unchecked")
		    Enumeration<String> enums = (Enumeration<String>) mProps.propertyNames();
		    while (enums.hasMoreElements()) {
		      String key = enums.nextElement();
		      String value = mProps.getProperty(key);
		      System.setProperty(key, value);
		    }		    
		} catch(Exception ex) {
			System.err.println("Error al cargar el archivo de propiedades " + fileName);
		}
		
	}
}
