package ar.com.menhir.WSCLob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import ar.com.menhir.db.DBConn;
import ar.com.menhir.db.WSAAdbo;
import ar.com.menhir.db.WSProperties;

public class WSMsg {
	private long oTransact;
	private String vServicio;
	private String vMetodo;
	private Date fInicio;
	private Date fFin;
	private String xlEnviar;
	private String xlRecibir;
	private long nResponse;
	
	final static Logger logger = Logger.getLogger(WSMsg.class);
	
	private WSMsg(long oTransact, String vServicio, String vMetodo, String xlEnviar) {
		super();
		this.oTransact = oTransact;
		this.vServicio = vServicio;
		this.vMetodo = vMetodo;
		this.xlEnviar = xlEnviar;
	}
	
	public static WSMsg[] getWMsgs() throws Exception {
		
		ArrayList<WSMsg> retVal = new ArrayList<WSMsg>(); 
		
		String strSQL = "select * from web_ws_msgs where m_procesado is null order by o_transact";
		logger.debug(strSQL);
		Statement s = DBConn.openConn().createStatement();
		ResultSet rs = s.executeQuery(strSQL);
		while (rs.next()) {
			retVal.add(new WSMsg( rs.getLong("o_transact")
					            , rs.getString("v_servicio")
					            , rs.getString("v_metodo")
					            , rs.getString("xl_enviar")));
		}
		rs.close();
		s.close();
		WSMsg[] ret = new WSMsg[retVal.size()];
		ret = retVal.toArray(ret);
		return ret;
	}
	
	protected static boolean sslHandlerInstalled = false;
	
	protected static void setHTTPSIgnoreCertErrors() throws Exception {
		if ( sslHandlerInstalled )
			return;
		sslHandlerInstalled = true;
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
					
				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {					
				}
			} 
		};
        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
 
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
 
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);		
	}

	protected HttpURLConnection getConn(String ns) throws Exception {
		setHTTPSIgnoreCertErrors();
		URL url = new URL(WSProperties.getWSEndpoint(this.vServicio));
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Accept-Encoding", "gzip,deflate");
		conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		String prop = ns + this.vMetodo;
		conn.setRequestProperty("SOAPAction", prop);
		prop = url.getHost();
		conn.setRequestProperty("Host", prop);
		return conn;
	}
	
	private static PreparedStatement lockStm = null;
	protected boolean lock() {
		boolean retVal = true;
		String SQL = 
				"select m_procesado\n" +
		        "  from web_ws_msgs\n" +
				"where o_transact = ?\n" +
				"  and m_procesado is null\n" +
				"   for update of m_procesado\n";
		try {
			if ( lockStm == null )
				lockStm = DBConn.openConn().prepareStatement(SQL);
			lockStm.setLong(1, this.oTransact);
			ResultSet rs = lockStm.executeQuery();
			retVal = rs.next();
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;		
	}
	
	private InputStream applyDecompressionIfApplicable(HttpURLConnection conn, InputStream in) throws IOException {
		if (in != null && "gzip".equals(conn.getContentEncoding().toLowerCase())) {
			return new GZIPInputStream(in);
		}
		return in;
	}

	public void send() throws Exception {
		try {
			logger.info("Conectando a WSAA");

			WSAAdbo mLogin = new WSAAdbo(this.vServicio);
			
			// Agrego el ns
			String ns = WSProperties.getPropertyVal(this.vServicio + ".ns");
			
			
			String input = 
					"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"" + ns + "\">" +
					"   <soapenv:Header/>" +
					"   <soapenv:Body>" +
					"<ns:" + this.vMetodo + ">" +
					mLogin.getAuth() +
					this.xlEnviar +
					"</ns:" + this.vMetodo + ">" +
					"   </soapenv:Body>" +
					"</soapenv:Envelope>";
			logger.info(input);
			HttpURLConnection conn = getConn(ns);
			this.fInicio = new Date();
			this.saveToDB();
			OutputStream os = conn.getOutputStream();
			logger.info("Enviando al servidor");
			os.write(input.getBytes());
			os.flush();
			
			nResponse = conn.getResponseCode();
			
			logger.info("Recibiendo del servidor");
			conn.getContentEncoding();
						
			BufferedReader br = new BufferedReader(new InputStreamReader((applyDecompressionIfApplicable(conn,conn.getInputStream()))));

			String output;
			String retData = "";
			while ((output = br.readLine()) != null) {
				retData += output;
			}
			logger.info(retData);
			
			for (Map.Entry<String, List<String>> entries : conn.getHeaderFields().entrySet()) {
				String values = "";
				for (String value : entries.getValue()) {
					values += value + ",";
				}
				logger.debug("Response header: "+ entries.getKey() + " - " +  values );
			}			
			
			conn.disconnect();
			
			
			Reader tokenReader = new StringReader(retData);
			SAXReader rdr = new SAXReader(false);
			
			Document tokenDoc = rdr.read(tokenReader);
			Namespace inpNs = tokenDoc.getRootElement().getNamespace();
			@SuppressWarnings("unchecked")
			List<Node> nodes = tokenDoc.selectNodes( "/" + inpNs.getPrefix() + ":Envelope/"+ inpNs.getPrefix() +":Body/*" );
			this.fFin = new Date();
			this.xlRecibir = ((Node)nodes.get(0)).asXML();
			this.saveToDB();
			
		} catch (Exception e) {
			logger.error("Error a grabar en la base de datos:" + e.getMessage());
			e.printStackTrace();
			throw e;
		}
		
	}

	public long getoTransact() {
		return oTransact;
	}

	public void setoTransact(long oTransact) {
		this.oTransact = oTransact;
	}

	public String getvServicio() {
		return vServicio;
	}

	public void setvServicio(String vServicio) {
		this.vServicio = vServicio;
	}

	public String getvMetodo() {
		return vMetodo;
	}

	public void setvMetodo(String vMetodo) {
		this.vMetodo = vMetodo;
	}

	private void saveToDB() throws Exception {
		String SQL = 
				"update web_ws_msgs set\n" +
				"	    fec_inicio = ?\n" +
				"	   ,fec_fin = ?\n" +
				"	   ,xl_recibir = ?\n" +
				"	   ,m_procesado = ?\n" +
				"	   ,n_response = ?\n" +
				"where o_transact = ?\n";
		PreparedStatement ps = DBConn.openConn().prepareStatement(SQL);
		if ( this.fInicio == null )
			ps.setTimestamp(1, null);
		else
			ps.setTimestamp(1, new java.sql.Timestamp(this.fInicio.getTime()));
		if ( this.fFin == null )
			ps.setTimestamp(2, null);
		else
			ps.setTimestamp(2, new java.sql.Timestamp(this.fFin.getTime()));
		ps.setString(3, this.xlRecibir);
		ps.setString(4, "S");
		ps.setLong(5, this.nResponse);
		ps.setLong(6, this.oTransact);
		ps.execute();
		ps.close();
		
	}
	
}
