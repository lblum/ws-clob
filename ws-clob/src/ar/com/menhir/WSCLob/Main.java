package ar.com.menhir.WSCLob;


import org.apache.log4j.Logger;

import ar.com.menhir.WSAA.Login;
import ar.com.menhir.db.FileProperties;
import ar.com.menhir.db.WSProperties;

public class Main {
	final static Logger logger = Logger.getLogger(Login.class);

	public static void main(String[] args) {
		if ( args.length <= 0 ) {
			System.err.println("Uso: java -jar ws-clob.jar <archivo de configuración>");
			System.exit(1);
		}
		try {
			FileProperties.loadPropFile(args[0]);
		} catch (Exception e1) {
			System.err.println("Error al cargar el archivo de propiedades " + args[0]);
			System.exit(1);
		}
		// Chequeo que estén ok todos los parámetros
		try {
			if ( WSProperties.getDBURL() == null || 
				 WSProperties.getDBUser() == null ||
				 WSProperties.getDBPass() == null )
				usage();
			// Loop
			logger.info("Comienzo del proceso");
			WSMsg msgs[] =  WSMsg.getWMsgs();
			if ( msgs.length == 0 ) {
				logger.info("No hay registros para transmitir");
			} else {
				
				int i=1;
				for ( WSMsg msg : msgs ) {
					String txt;
					if ( msg.lock() ) {
						txt = "Enviando el registro " + msg.getoTransact() + " a " + msg.getvServicio() + "/" + msg.getvMetodo() + " (" + i + "/" + msgs.length + ")";
						logger.info(txt);
						msg.send();
					} else {
						txt = "El registro " + msg.getoTransact() +  " (" + i + "/" + msgs.length + ") ha sido tomado y/o enviado por otro proceso";
						logger.info(txt);						
					}
					i++;
				}
				logger.info("Se enviaron un total de "+ msgs.length + " registro(s)");			
			}
		} catch (Exception e) {
			logger.error(e);
		}
		logger.info("Fin del proceso");

	}

	private static void usage() {
		String usage = "Parámetros insuficientes:\n" +
					   "Debe definir los siguientes parámetros en el archivo de configuración:\n" +
					   "si usa SID: dbURL=jdbc:oracle:thin:@<servidor>:<puerto>:<SID>\n" +
					   "si usa servicio: dbURL=jdbc:oracle:thin:@//<servidor>:<puerto><servicio>\n" +
					   "dbUser=<usuario de base de datos>\n" +
					   "dbPass=<password\n";
		System.err.println(usage);
		System.exit(1);
	}

}
