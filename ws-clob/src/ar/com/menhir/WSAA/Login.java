package ar.com.menhir.WSAA;
/**
 * 
 */


import java.io.Reader;
import java.io.StringReader;
import java.util.Date;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.axis.encoding.Base64;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import ar.com.menhir.WSAA.AFIP.LoginCMS;
import ar.com.menhir.WSAA.AFIP.LoginCMSProxy;
import ar.com.menhir.WSAA.utils.PKSigner;
import ar.com.menhir.WSAA.utils.Signer;
import ar.com.menhir.WSAA.utils.XMLLogin;
/**
 * @author lblum
 *
 */
public class Login {
	final static Logger logger = Logger.getLogger(Login.class);

	/*
	 * La URL donde est� el WS de autorizaci�n 
	 */
	private String mEndPoint="";
	
	/*
	 * Archivo de certificado 
	 */
	private String mCertFile="";
	
	/*
	 * Password de extracci�n del certificado
	 */
	private String mPasswd = "";
	
	/*
	 * La CUIT 
	 */
	long mCUIT = 0L;
	
	/*
	 * El token recibido
	 */
	protected String mToken=null;
	
	/*
	 * La firma recibida
	 */
	protected String mSign=null;
	
	/*
	 * La hora de expiraci�n
	 */
	protected Date mExpDate = null;
	
	
	/**
	 * @return the mEndPoint
	 */
	public String getEndPoint() {
		return mEndPoint;
	}


	/**
	 * @param mEndPoint the mEndPoint to set
	 */
	public void setEndPoint(String mEndPoint) {
		this.mEndPoint = mEndPoint;
	}


	/**
	 * @return the mService
	 */
	public String getService() {
		return this.xmlLogin.getService();
	}


	/**
	 * @param mService the mService to set
	 */
	public void setService(String mService) {
		this.xmlLogin.setService(mService);
	}


	/**
	 * @return the mToken
	 */
	public String getToken() throws Exception {
		if ( isExpired() )
			doLogin();	
		return mToken;
	}


	/**
	 * @return the mSign
	 */
	public String getSign() throws Exception {
		if ( isExpired() )
			doLogin();
		return mSign;
	}


	/**
	 * @return the mCUIT
	 */
	public long getCUIT() {
		return mCUIT;
	}


	/**
	 * @param mCUIT the mCUIT to set
	 */
	public void setCUIT(long mCUIT) {
		this.mCUIT = mCUIT;
	}


	/*
	 * Clases auxiliares
	 */
	XMLLogin xmlLogin = null;
	Signer pkSigner = null;
	

	/**
	 * @param certFile
	 * 	Archivo de certificado X509
	 * @param passwd
	 * 	Password de extracci�n
	 */
	public Login(String certFile, String passwd) throws Exception {
		super();
		this.mCertFile = certFile;
		this.mPasswd = passwd;
		pkSigner = new PKSigner(mCertFile, mPasswd);
		xmlLogin = new XMLLogin();
		mExpDate = new Date();
	}
	
	protected boolean isExpired(){
		if ( mSign==null|| mToken == null )
			return true;
		Date now = new Date();
		return mExpDate.before(now); 
	}
	
	protected void doLogin() throws Exception {

		// Armo el xml de conexi�n
		byte[] xmlOut=pkSigner.signString(xmlLogin.getXMLString());
		
		// Me conecto al WS
		logger.info("Estableciendo conexi�n de autorizaci�n");
		LoginCMS wsWSAA = new LoginCMSProxy(mEndPoint);	
		String strOut = wsWSAA.loginCms(Base64.encode(xmlOut));

		logger.info("Conexi�n de autorizaci�n finalizada");
		Reader tokenReader = new StringReader(strOut);
		Document tokenDoc = new SAXReader(false).read(tokenReader);
		// Si lleg� ac� es que pas� OK, separo el token y la firma	
		DatatypeFactory dtf=DatatypeFactory.newInstance();
		XMLGregorianCalendar expTime = dtf.newXMLGregorianCalendar(			
			tokenDoc.valueOf("/loginTicketResponse/header/expirationTime"));
		this.mExpDate = expTime.toGregorianCalendar().getTime();
		this.mToken = tokenDoc.valueOf("/loginTicketResponse/credentials/token");
		this.mSign = tokenDoc.valueOf("/loginTicketResponse/credentials/sign");
		
	}
	

}
