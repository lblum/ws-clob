/**
 * 
 */
package ar.com.menhir.WSAA.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author lblum
 *
 */
public class MiscUtils {
	public static String DateToXML(Date d){
		GregorianCalendar gentime = new GregorianCalendar();
		gentime.setTime(d);
		return DateToXML(gentime);
	}
	
	public static String DateToXML(GregorianCalendar gentime){
		
		try {
			XMLGregorianCalendar XMLGenTime;
			XMLGenTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gentime);
			return XMLGenTime.toString();
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static String DateToAnsi(Date d){
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
		return f.format(d);
	}
	
	/*
	public static String DateToXML(java.sql.Date d){
		String res="";
		
		return res;
	}
	*/
}
