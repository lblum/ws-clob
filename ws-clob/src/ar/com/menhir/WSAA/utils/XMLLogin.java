/**
 * 
 */
package ar.com.menhir.WSAA.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;


/**
 * @author lblum
 *
 */
public class XMLLogin {


	private String mService=null;
	private int mTicketTime=6;
	private Random mUniqueID=null;

	/**
	 * @return the service
	 */
	public String getService() {
		return mService;
	}

	/**
	 * @param mService the mService to set
	 */
	public void setService(String mService) {
		this.mService = mService;
	}
	
	/**
	 * @return the ticketTime
	 */
	public long getTicketTime() {
		return mTicketTime;
	}
	/**
	 * @param ticketTime the ticketTime to set
	 */
	public void setTicketTime(int ticketTime) {
		this.mTicketTime = ticketTime;
	}
	
	
	public XMLLogin( ) {
		super();
		mUniqueID = new Random();
	}
	
	public String getXMLString(){
		String xmlRet = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		xmlRet += "<loginTicketRequest version=\"1.0\">\n";
		xmlRet += "	<header>\n";
		xmlRet += "		<uniqueId>" + Math.abs(mUniqueID.nextInt()) + "</uniqueId>\n";
		
		Date GenTime = new Date();
		GregorianCalendar gentime = new GregorianCalendar();
		GregorianCalendar exptime = new GregorianCalendar();
		
		exptime.setTime(GenTime);
		gentime.setTime(GenTime);
		exptime.add(Calendar.HOUR, mTicketTime);
		gentime.add(Calendar.HOUR, -mTicketTime);
		
		xmlRet += "		<generationTime>" + MiscUtils.DateToXML(gentime) + "</generationTime>\n";
		xmlRet += "		<expirationTime>" + MiscUtils.DateToXML(exptime)  + "</expirationTime>\n";
		xmlRet += "	</header>\n";
		xmlRet += "	<service>" + mService + "</service>\n";
		xmlRet += "</loginTicketRequest>\n";;

		return xmlRet;
	}
	
}
