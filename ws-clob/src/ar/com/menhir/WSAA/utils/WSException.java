/**
 * 
 */
package ar.com.menhir.WSAA.utils;

/**
 * @author lblum
 *
 */
public class WSException extends Exception {

	/*
	 * El c�digo de error
	 */
	private long mErrCode=-1;
	
	
	/**
	 * 
	 */
	public WSException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public WSException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public WSException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public WSException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the mErrCode
	 */
	public long getErrCode() {
		return mErrCode;
	}

	/**
	 * @param mErrCode the mErrCode to set
	 */
	public void setErrCode(long mErrCode) {
		this.mErrCode = mErrCode;
	}

	/**
	 * @param mErrCode
	 */
	public WSException(String arg0, long mErrCode) {
		super(arg0);
		this.mErrCode = mErrCode;
	}
	
	/**
	 * @param mErrCode
	 */
	public WSException(String arg0, long mErrCode,Throwable arg1) {
		super(arg0, arg1);
		this.mErrCode = mErrCode;
	}

	@Override
	public String toString() {
		return getErrCode()+":"+getMessage();
	}
	
	

}
