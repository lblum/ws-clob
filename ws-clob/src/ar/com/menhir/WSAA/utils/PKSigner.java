package ar.com.menhir.WSAA.utils;

import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class PKSigner extends Signer {

	@Override
	protected CertStore getCStore(ArrayList<X509Certificate> certList) throws Exception{

		if (Security.getProvider("BC") == null) {
			Security.addProvider(new BouncyCastleProvider());
		}

		return CertStore.getInstance("Collection", new CollectionCertStoreParameters (certList), "BC");
	}

	@Override
	protected KeyStore getKeyStore() throws Exception {
		return KeyStore.getInstance("pkcs12");
	}

	/**
	 * @param certFile
	 * @param certPass
	 * @throws Exception
	 */
	public PKSigner(String certFile, String certPass) throws Exception {
		super(certFile, certPass);
	}
	
	

}
