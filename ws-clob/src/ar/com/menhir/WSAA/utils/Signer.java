/**
 * 
 */
package ar.com.menhir.WSAA.utils;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;

import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;

/*
 * TODO Documentar y JavaDOC
 */

/**
 * @author lblum
 *
 */
public abstract class Signer {
	protected PrivateKey pKey = null;
	protected X509Certificate pCertificate = null;
	protected CertStore cstore = null;

	protected abstract CertStore getCStore(ArrayList<X509Certificate> certList) throws Exception;
	protected abstract KeyStore getKeyStore() throws Exception;
	
	public Signer(String certFile , String certPass ) throws Exception{
		KeyStore ks = getKeyStore();
		FileInputStream p12stream = new FileInputStream ( certFile ) ;
		ks.load(p12stream, certPass.toCharArray());
		p12stream.close();
		
		
        Enumeration<String> aliasesEnum = ks.aliases();
        if (aliasesEnum.hasMoreElements()) {
            String alias = (String)aliasesEnum.nextElement();
            pCertificate = (X509Certificate)ks.getCertificate(alias);
            pKey = (PrivateKey) ks.getKey(alias, certPass.toCharArray());
        } else {
            throw new KeyStoreException("El almacen de claves est� vac�o.\n");
        }
		// Create a list of Certificates to include in the final CMS
		ArrayList<X509Certificate> certList = new ArrayList<X509Certificate>();
		certList.add(pCertificate);
		cstore = getCStore(certList);
	}
	
	public byte[] signString ( String strIn ) throws Exception{
		//
		// Create CMS Message
		//
		byte[] result;
		// Create a new empty CMS Message
		CMSSignedDataGenerator gen = new CMSSignedDataGenerator();

		// Add a Signer to the Message
		gen.addSigner(pKey, pCertificate, CMSSignedDataGenerator.DIGEST_SHA1);

		// Add the Certificate to the Message
  		gen.addCertificatesAndCRLs(cstore);

		// Add the data (XML) to the Message
		CMSProcessable data = new CMSProcessableByteArray(strIn.getBytes());

		// Add a Sign of the Data to the Message
		CMSSignedData signed = gen.generate(data, true, "BC");	

		// 
		result = signed.getEncoded();
		return result;
	}
	
}
