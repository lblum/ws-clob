SET DEFINE OFF;

drop table web_ws_wsaa	;
drop table web_ws_msgs	;
drop sequence s_web_ws_msgs_pk;
drop sequence s_web_ws_wsaa_pk;
create sequence s_web_ws_msgs_pk
nomaxvalue
nominvalue
nocycle;
create sequence s_web_ws_wsaa_pk
nomaxvalue
nominvalue
nocycle;
drop table web_ws_param;

create table web_ws_wsaa	
   (o_id integer not null
   ,v_servicio varchar2(40 byte) not null
   ,v_token    varchar2(4000 byte) not null
   ,v_sign    varchar2(4000 byte) not null
  ,fec_expira  date not null
  ,fec_alta date not null
  ,usu_alta varchar2(30) not null
  ,fec_modi date
  ,usu_modi varchar2(30)  
);

create or replace trigger trg_on_insert_web_ws_wsaa
before insert on web_ws_wsaa
referencing new as new old as old 
for each row
begin
  :new.fec_alta := sysdate;
  :new.usu_alta := user;
  select s_web_ws_wsaa_pk.nextval
    into :new.o_id
    from dual;
end;
/

create or replace trigger trg_on_update_web_ws_wsaa
before update on web_ws_wsaa
referencing new as new old as old 
for each row
begin
  :new.fec_modi  := sysdate;
  :new.usu_modi := user;
end;
/

create table web_ws_msgs 
( o_transact int not null 
, m_procesado varchar2(1) null  
, v_servicio varchar2(40 byte) not null
, v_metodo varchar2(40 byte) not null
, n_response int
, fec_inicio date  
, fec_fin date 
, xl_enviar clob 
, xl_recibir clob 
, fec_alta date not null
, usu_alta varchar2(30) not null
, fec_modi date
, usu_modi varchar2(30)
, primary key(o_transact)
);

comment on column web_ws_msgs.o_transact is 'Clave primaria (de la secuencia s_web_ws_msgs)';

comment on column web_ws_msgs.m_procesado is 'Marca de procesado';

comment on column web_ws_msgs.n_response is 'C�digo HTTP de respuesta del servidor';

comment on column web_ws_msgs.fec_inicio is 'Fecha y hora de inicio';

comment on column web_ws_msgs.fec_fin is 'Fecha y hora de fin';

comment on column web_ws_msgs.xl_enviar is 'Mensaje a enviar';

comment on column web_ws_msgs.xl_recibir is 'Mensaje recibido';

create or replace trigger trg_on_insert_web_ws_msgs 
before insert on web_ws_msgs 
referencing new as new old as old 
for each row
begin
  :new.fec_alta  := sysdate;
  :new.usu_alta := user;
  select s_web_ws_msgs_pk.nextval
    into :new.o_transact
    from dual;
end;
/

create or replace trigger trg_on_update_web_ws_msgs
before update on web_ws_msgs
referencing new as new old as old 
for each row
begin
  :new.fec_modi  := sysdate;
  :new.usu_modi := user;
end;
/

create table web_ws_param
  (v_param_name varchar2(80 byte) not null
  ,v_param_val  varchar2(4000 byte) not null
  ,x_param_desc varchar2(4000 byte) not null
  ,fec_alta date not null
  ,usu_alta varchar2(30) not null
  ,fec_modi date
  ,usu_modi varchar2(30)
  ,primary key (v_param_name)
);

create or replace trigger trg_on_insert_web_ws_param
before insert on web_ws_param 
referencing new as new old as old 
for each row
begin
  :new.fec_alta := sysdate;
  :new.usu_alta := user;
end;
/

create or replace trigger trg_on_update_web_ws_param
before update on web_ws_param 
referencing new as new old as old 
for each row
begin
  :new.fec_modi := sysdate;
  :new.usu_modi := user;
end;
/

insert into web_ws_param (V_PARAM_NAME,V_PARAM_VAL,X_PARAM_DESC) values ('cert.fileName','cert/desa/lb-certificado.p12','archivo de certificado digital');
insert into web_ws_param (V_PARAM_NAME,V_PARAM_VAL,X_PARAM_DESC) values ('cert.filePass','1234','password del archivo de certificado digital');
insert into web_ws_param (V_PARAM_NAME,V_PARAM_VAL,X_PARAM_DESC) values ('wsfe.endpoint','https://wswhomo.afip.gov.ar/wsfev1/service.asmx','endpoint de factura electronica');
insert into web_ws_param (V_PARAM_NAME,V_PARAM_VAL,X_PARAM_DESC) values ('wsfe.ns','http://ar.gov.afip.dif.FEV1/','namespace de wsfe');
insert into web_ws_param (V_PARAM_NAME,V_PARAM_VAL,X_PARAM_DESC) values ('wsaa.endpoint','https://wsaahomo.afip.gov.ar/ws/services/LoginCms','endpoint de wsaa' );
insert into web_ws_param (V_PARAM_NAME,V_PARAM_VAL,X_PARAM_DESC) values ('cuit','20164974058','CUIT' );
